$(function() {
    
    // Menu responsive
    
    function ResponsiveMenu() {
        
        $('.show-menu').click(function(e) {
            
            if($('#menu').is(':visible')) {
                
            $(this).find('i').removeClass('fa-times').addClass('fa-bars');
            $('#menu').slideUp(function() {
                $('.overlay').fadeOut();
            });   
                
            } else {
                
            $(this).find('i').removeClass('fa-bars').addClass('fa-times');
            $('#menu').slideDown(function() {
                $('.overlay').fadeIn();
            });
                
            }
            
            e.preventDefault();
            
        });
        
    }
    
    ResponsiveMenu();
    
    $(window).resize(function() {
        
        if($(this).width() > 768 && $('#menu').is(':hidden')) {
            
            $('#menu').removeAttr('style');
            
        }
        
    });
    
    

});